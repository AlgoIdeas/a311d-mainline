#!/bin/bash

# Functions:
# unset_package_vars

PKG_BUILD=.

## $1 package name
patch_package() {

		for i in /homeB/minjie.yu/amlogic/mainline/uboot/u-boot-mainline/patches/v2021.04/*.patch; do

			if [ -f "$i" ]; then
				if [ -n "$(grep -E '^GIT binary patch$' $i)" ]; then
					cat $i | git apply --directory=`echo "$PKG_BUILD" | cut -f1 -d\ ` -p1 --verbose --whitespace=nowarn --unsafe-paths
				else
					echo "[PATCH] $i ::"
					#cat $i | patch -d `echo "$PKG_BUILD" | cut -f1 -d\ ` -p1
					patch -d "$PKG_BUILD" -p1 < "$i"
				fi
			fi
		done

	return 0
}

patch_package
