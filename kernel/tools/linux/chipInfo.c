#define _GNU_SOURCE
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>
#include <errno.h>
#include <unistd.h>


#define AML_DT_MAGIC     "AML_"  /* Master DTB magic */
#define AML_DT_VERSION   2       /* AML version */

#define DT_ID_TAG    "amlogic-dt-id"

#define PAGE_SIZE_DEF  2048
#define PAGE_SIZE_MAX  (1024*1024)

#define log_err(x...)  printf(x)
#define log_info(x...) printf(x)
#define log_dbg(x...)  { if (verbose) printf(x); }

#define COPY_BLK       1024    /* File copy block size */

#define RC_SUCCESS     0
#define RC_ERROR       -1

#define INFO_ENTRY_SIZE     16
#define INFO_ENTRY_SIZE_S  "16"
#define TABLE_ENTRY_HEADER_SIZE (INFO_ENTRY_SIZE * 3 + sizeof(uint32_t) * 2)

struct chipInfo_t {
  uint8_t chipset[INFO_ENTRY_SIZE];
  uint8_t platform[INFO_ENTRY_SIZE];
  uint8_t revNum[INFO_ENTRY_SIZE];
  uint32_t dtb_size;
  char     *dtb_file;
  struct chipInfo_t *prev;
  struct chipInfo_t *next;
};


/* Extract 'qcom,msm-id' parameter triplet from DTB
      qcom,msm-id = <x y z>;
 */
struct chipInfo_t *getChipInfo(const char *filename)
{
    const char str1[] = "dtc -I dtb -O dts \"";
    const char str2[] = "\" 2>&1";
    char *buf, *pos;
    char *line = NULL;
    size_t line_size;
    FILE *pfile;
    int llen;
    struct chipInfo_t *chip = NULL;
    int rc = 0;
    uint8_t data[3][INFO_ENTRY_SIZE + 1] = { {0} };

    line_size = 1024;
    line = (char *)malloc(line_size);
    if (!line) {
        log_err("Out of memory\n");
        return NULL;
    }

    llen = sizeof(char) * (strlen(str1) +
                           strlen(str2) +
                           strlen(filename) + 1);
    buf = (char *)malloc(llen);
    if (!buf) {
        log_err("Out of memory\n");
        free(line);
        return NULL;
    }

    strncat(buf, str1, llen);
    strncat(buf, filename, llen);
    strncat(buf, str2, llen);

    pfile = popen(buf, "r");
    free(buf);

    if (pfile == NULL) {
        log_err("... skip, fail to decompile dtb\n");
    } else {
        /* Find "dtb file entry" */
        while ((llen = getline(&line, &line_size, pfile)) != -1) {
            if ((pos = strstr(line, DT_ID_TAG)) != NULL) {
                pos += strlen(DT_ID_TAG);

                for (; *pos; pos++) {
                    if (*pos == '"') {
                        /* Start convertion of triplet */
                        pos++;
                        rc = sscanf(pos, "%" INFO_ENTRY_SIZE_S "[^_]_%" INFO_ENTRY_SIZE_S "[^_]_%" INFO_ENTRY_SIZE_S "[^_\"]\"",
                                    data[0], data[1], data[2]);
                        if (rc == 3) {
                            chip = (struct chipInfo_t *)
                                       malloc(sizeof(struct chipInfo_t));
                            if (!chip) {
                                log_err("Out of memory\n");
                                break;
                            }
                            memcpy(chip->chipset, data[0], INFO_ENTRY_SIZE);
                            memcpy(chip->platform, data[1], INFO_ENTRY_SIZE);
                            memcpy(chip->revNum, data[2], INFO_ENTRY_SIZE);
                            chip->dtb_size = 0;
                            chip->dtb_file = NULL;

                            free(line);
                            pclose(pfile);
                            return chip;
                        } else {
                            break;
                        }
                    }
                }
                log_err("... skip, incorrect '%s' format\n", DT_ID_TAG);
                break;
            }
        }
        if (line)
            free(line);
        pclose(pfile);
    }

    return NULL;
}

int main(int argc, char *argv[])
{
  struct chipInfo_t *info = getChipInfo(argv[1]);
  if (NULL == info) {
    log_err("info is NULL!\n");
    return -1;
  }

  log_info("chipset: %" INFO_ENTRY_SIZE_S "s, "
           "platform: %" INFO_ENTRY_SIZE_S "s, "
           "rev: %" INFO_ENTRY_SIZE_S "s\n",
           info->chipset, info->platform, info->revNum);

  if (NULL != info) {
    free(info);
  }

  return 0;
}